(function() {
  const URL = 'https://cors-anywhere.herokuapp.com/https://byrides.eu.auth0.com/oauth/token';
  const LOGIN = 'http://159.65.117.77/AgentLogin/Post';
  const TOKEN_OBJ = {
    'client_id': '3txzYMEkRSrztiGtbvFvoEAhxQdKPVpp',
    'client_secret': 'Nz8acL5_nENRfX_hJ9YVMCVHzk9rvmEKbe4Vz7f2Ui5oIrMY-P160FB-xpQqsSd8',
    'audience': 'https://byrides-api-auth',
    'grant_type': 'client_credentials'
  };
  

  var form = document.getElementById("login-form");
  form.addEventListener("submit", function(e) {
    e.preventDefault();
    var data = stringifyFormData(this);
    postData(LOGIN, data).then(resp => {
      return resp.status == 200
        ? resp.json()
        : {}
    }).then(resp => console.log(resp)).catch(err => console.log(err));

  });

  function stringifyFormData(formObj) {
    var formData = {};
    var data = new FormData(formObj);
    for (const entry of data) {
      formData[entry[0]] = entry[1];
    };
    return JSON.stringify(formData)
  }

  function getAccessToken() {
    return new Promise(function(resolve, reject) {
      if (getCookie("accessTokenBearer"))
        resolve(getCookie("accessTokenBearer"));
      else {
        fetch(URL, {
          body: JSON.stringify(TOKEN_OBJ),
          method: 'POST',
          headers: {
            'content-type': 'application/json'
          }
        }).then((r) => r.json()).then(data => {
          setCookie("accessTokenBearer", data.access_token, data.expires_in);
          console.log(data)
          resolve(getCookie("accessTokenBearer"));

        }).catch(e => {
          console.log(e);
          //setCookie("accessTokenBearer", data.access_token, data.expires_in);
          reject(e);

        });
      }
    });

  }

  function postData(url, data) {
    return new Promise(function(resolve, reject) {
      getAccessToken().then(function(token) {
        // Default options are marked with *
        return fetch(url, {
          method: 'POST',
          body: data, // must match 'Content-Type' header
          headers: {
            'Authorization': 'Bearer ' + token,
            'content-type': 'application/json',
            'accept': 'application/json'
          }, // *GET, POST, PUT, DELETE, etc.
        }).then(resp => {
          console.log(resp);
          resolve(resp)
        }).catch(err => reject(err)) // parses response to JSON
      }).catch(err => reject(err));
    })
  }

  function setCookie(name, value, time) {
    var expires = "";
    if (time) {
      var date = new Date();
      date.setTime(date.getTime() + time);
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (
    value || "") + expires + "; path=/";
  }
  function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ')
        c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0)
        return c.substring(nameEQ.length, c.length);
      }
    return null;
  }
  function eraseCookie(name) {
    document.cookie = name + '=; Max-Age=-99999999;';
  }

}());
