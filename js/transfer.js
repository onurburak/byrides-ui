"use strict";
(function() {
	let PRICE_HOLDER;
	let FINAL_PRICE = 0;
	const URL = "http://159.65.117.77";
	const SEARCH_OBJ = {
		cityId: "",
		terminalId: "",
		destinationId: "",
		oneWay: "",
		serviceTime: "",
		groupSize: "",
		agentId: ""
	};
	const EXTRA_OBJ = [];
	const PRODUCT_OBJ = {
		product_id: "",
		product_option_id: ""
	};

	const BOOKING_OBJ = {
		id: 0, //id 0 olacak
		groupSize: 4,
		customerName: "Bugra Sitemkar",
		hotelName: "Kadıköy Rıhtım Otel",
		hotelAddress: "Kadıköy Rıhtım",
		serviceDate: "2018-07-09T12:56:29.086Z",
		bookingDate: "2018-07-09T12:56:29.086Z",
		departurePickupDate: "2018-07-09T12:56:29.086Z", //yalnızca departure için geçerli, dönüş için ne zaman alınmak istiyor, arrival kayıtları için boş
		flightNumber: "TK123",
		salePrice: 150,
		reservationStatusId: 1, //bunu her zaman 1 set edebilirsin
		agentId: 3,
		customerContactPhone: "555 55 55",
		customerContactChannelId: 1,
		productId: 6,
		productOptionId: 5,
		selectedAdditionalServices: [], //product options altındaki additional servislerden müşterinin seçtiklerinin idsi
		paymentDueDate: "", //boş olacak
		bookingGroupId: "string", ////eğer sadece arrival veya sadece departure için birer kayıt atıyorsan bu alan boş olacak, eğer iki kayıt atıyorsan
		////bir arrival bir departure için, random bir guid oluşturup buraya attığın iki kayıt için de aynı guid i bu alana yazarsan ben oradan ayırt edebilicem.
		isArrival: true, // arrival kaydı için true, departure kaydı için false
		currencyId: 6
	};

	let setAll = (obj, val) => Object.keys(obj).forEach(k => (obj[k] = val));
	let setNull = obj => setAll(obj, null);

	var contentWayPoint = function() {
		var i = 0;
		$(".animate-box").waypoint(
			function(direction) {
				if (!$(this.element).hasClass("animated-fast")) {
					i++;

					$(this.element).addClass("item-animate");
					setTimeout(function() {
						$("body .animate-box.item-animate").each(function(k) {
							var el = $(this);
							setTimeout(
								function() {
									var effect = el.data("animate-effect");
									if (effect === "fadeIn") {
										el.addClass("fadeIn animated-fast");
									} else if (effect === "fadeInLeft") {
										el.addClass("fadeInLeft animated-fast");
									} else if (effect === "fadeInRight") {
										el.addClass("fadeInRight animated-fast");
									} else {
										el.addClass("fadeInUp animated-fast");
									}

									el.removeClass("item-animate");
								},
								k * 200,
								"easeInOutExpo"
							);
						});
					}, 100);
				}
			},
			{ offset: "85%" }
		);
	};
	//function to init date picker
	var datePicker = function() {
		jQuery(".date").datepicker({ format: "MM d, yyyy", autoclose: true });
	};
	//function to init time picker

	var timePicker = function() {
		jQuery(".timepickerInput").timepicker({
			showMeridian: false,
			icons: {
				up: "icon icon-arrow-up2",
				down: "icon icon-arrow-down2"
			}
		});
	};
	//function to init sliders
	var sliderInit = () => {
		$(".slider").slider({
			animate: "fast",
			max: 30,
			create: function() {
				$(this).slider("value", $(this).attr("data-value"));
				$(this)
					.find(".ui-slider-handle")
					.text($(this).slider("value"));
			},
			slide: function(event, ui) {
				$(this)
					.find(".ui-slider-handle")
					.text(ui.value);
				$(this).data("value", ui.value);
			}
		});
	};

	var typeSuggestion = (url, id, key, attrList = []) => {
		var accentMap = {
			ş: "s",
			ü: "u",
			ö: "o",
			ğ: "g",
			i: "ı",
			ç: "c",
			s: "ş",
			u: "ü",
			o: "ö",
			g: "ğ",
			ı: "i",
			c: "ç"
		};
		var normalize = function(term) {
			var ret = "";
			for (var i = 0; i < term.length; i++) {
				ret += accentMap[term.charAt(i)] || term.charAt(i);
			}
			return ret;
		};

		fetch(url)
			.then(resp => resp.json())
			.then(function(listElements) {
				$(id).autocomplete({
					create: function() {
						$(this).data("ui-autocomplete")._renderItem = function(ul, item) {
							return $("<li>")
								.attr("data-value", item[key])
								.append(item[key])
								.appendTo(ul);
						};
					},
					source: function(request, response) {
						var matcher = new RegExp(
							$.ui.autocomplete.escapeRegex(request.term),
							"i"
						);
						response(
							$.grep(listElements, function(value) {
								value = value[key] || value.label || value.value || value;
								return matcher.test(value) || matcher.test(normalize(value));
							})
						);
					},
					select: function(event, ui) {
						$(id).val(ui.item[key]);
						attrList.map(attr => $(id).data(attr, ui.item[attr]));
						return false;
					}
				});
			})
			.catch(err => console.log(err));
	};

	//function for Search
	var searchTransfer = () => {
		$("#search-preferences-container>div").hide();

		var form = document.getElementById("transfer-form");

		function fillSearchFormObject() {
			setNull(SEARCH_OBJ);

			var airportId = $("#airport").data("id");
			var cityId = $("#airport").data("cityId");
			var destinationId = $("#destination").data("id");
			var arrivalDate = new Date($("#in-date").val());
			var arrivalTime = $("#in-time")
				.val()
				.split(":");
			arrivalDate.setHours(arrivalTime[0]);
			arrivalDate.setMinutes(arrivalTime[1]);

			var oneWay = $("input[name=oneWay]:checked").val() === "true";

			SEARCH_OBJ["cityId"] = cityId;
			SEARCH_OBJ["terminalId"] = airportId;
			SEARCH_OBJ["destinationId"] = destinationId;
			SEARCH_OBJ["serviceTime"] = arrivalDate.toISOString();
			SEARCH_OBJ["oneWay"] = oneWay;
			SEARCH_OBJ["groupSize"] = 2;
			SEARCH_OBJ["agentId"] = 2; // Login sonrası agentId bağlanacak

			if (!oneWay) {
				var departureDate = new Date($("#out-date").val());
				var departureDate = new Date($("#out-date").val());
				var departureTime = $("#out-time")
					.val()
					.split(":");
				departureDate.setHours(departureTime[0]);
				departureDate.setMinutes(departureTime[1]);
				departureDate = departureDate.toISOString();
			}

			return JSON.stringify(SEARCH_OBJ);
		}

		function getTransfers(data) {
			fetch(URL + "/api/search", {
				method: "POST",
				headers: {
					accept: "application/json",
					"content-type": "application/json"
				},
				body: data
			})
				.then(r => r.json())
				.then(r => productSelection(r))
				.catch(err => console.log(err));
		}
		$("input[name=oneWay]").on("change", function(e) {
			if (this.value != "true") {
				$("#inputForRound").show();
			} else {
				$("#inputForRound").hide();
			}
		});

		form.addEventListener(
			"submit",
			function(e) {
				e.preventDefault();
				// var data = fillSearchFormObject(); not need to stringify again
				var data = {
					cityId: 7,
					terminalId: 6,
					destinationId: 7,
					oneWay: false,
					arrivalServiceDate: "2018-07-08T20:51:40.827Z",
					departureServiceDate: "2018-08-08T20:51:40.827Z",
					groupSize: 3,
					agentId: 3
				};
				data = JSON.stringify(data);
				getTransfers(data);
			}.bind(form),
			false
		);
	};

	var productSelection = result => {
		var current_fs, next_fs, prev_fs; //fieldsets
		var left, top, opacity, scale; //fieldset properties which we will animate
		var animating; //flag to prevent quick multi-click glitches
		var product = result.product ? result.product : "";
		var productOptions = result.productOptions
			? result.productOptions.reduce((options, obj) => {
					options["vehicle-" + obj.id] = obj;
					return options;
			  }, {})
			: "";
		var checkFirstSearch = $("#search-results .list-group.active").hasClass(
			"service-list"
		);
		//First search check for slider
		if (!checkFirstSearch) {
			productFormGoBack(
				"#search-results .list-group.active",
				".vehicle-list-group"
			);
			// Search'i yeniden init()
			console.log("bu ilk sefer değil");
		} else {
			console.log("bu ilk sefer");

			$("#extras-button a").on("click", e => {
				e.preventDefault();
				showTransferDetailForm();
			});
			$("#product-final-form").on("submit", e => {
				e.preventDefault();
				// var data = fillBookingObject();
				var data = {};
				submitBookingOrder(data);
				console.log(data);
			});
			productFormNextStep(".service-list");
		} // İlk product screene dönüş

		console.log(result);
		console.log(productOptions);

		(function fillSideMenu() {
			var options = {
				year: "numeric",
				month: "short",
				day: "2-digit",
				hour: "2-digit",
				minute: "numeric",
				hour12: false
			};
			$("#search-preferences-container>div").fadeOut(400, () => {
				if (product) {
					var arrDate = product.arrivalServiceDate
						? new Date(product.arrivalServiceDate)
						: $("#in-date").val() + " " + $("#in-time").val();
					product.arrDate = arrDate.toLocaleDateString("en-US", options);
					$(".pref-dep")
						.show()
						.find("p")
						.text(product.terminalValue);
					$(".pref-dest")
						.show()
						.find("p")
						.text(product.destinationValue);
					$(".pref-indate")
						.show()
						.find("p")
						.text(product.arrDate);
					$(".pref-size")
						.show()
						.find("p")
						.text(product.groupSize);
					$(".pref-dist-time")
						.show()
						.find("p")
						.text(product.distance + " mins - " + product.duration + " km");

					if (!product.oneWay) {
						var depDate = product.departureServiceDate
							? new Date(product.departureServiceDate)
							: $("#out-date").val() + " " + $("#out-time").val();

						product.depDate = depDate.toLocaleString("en-US", options);
						$(".pref-outdate")
							.show()
							.find("p")
							.text(product.depDate);
					}
				} else {
					$(".pref-error").show();
				}
			});
		})();

		(function fillProductsList() {
			$(".vehicle-list-group").empty();
			for (var option in productOptions) {
				var vehicle = productOptions[option];
				const markup = `<div class="col-sm-12 animate-box" id="vehicle-${
					vehicle.id
				}">
          <div class="list-group-item">
            <div class="row">
              <div class="list-headline">
                <div class="col-xs-12 col-sm-3 vehicle-img">
                  <img src="http://motors.stylemixthemes.com/rent-a-car/wp-content/uploads/sites/7/2017/01/minivan_van-300x181.png" alt="">
                </div>
                <div class="col-xs-12 col-sm-9 vehicle-content">
                  <div class="col-md-6 col-sm-6">
                    <div class="vehicle-title">
                      <h2>${vehicle.serviceTypeValue +
												" " +
												vehicle.vehicleTypeValue}</h2>
                      <h5>Volkswagen Transit or Similar</h5>
                    </div>
                    <div class="vehicle-info">
                      <div class="row">
                        <div class="infos">
                          <div><span><i class="icon icon-user"></i>${
														vehicle.seatCapacity
													} SEATS</span></div>
                          <div><span><i class="icon icon-briefcase"></i>${
														vehicle.luggageCapacity
													} BAGS</span></div>
                          <div><span><i class="icon icon-clock"></i>A/C</span></div>
                        </div>
                      </div>
                    </div>
                    <div class="vehicle-more-info">
                      <a href="#">
                        More Information
                        <div class="arrow-down"></div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="vehicle-price">
                      <div class="normal-price">
                        <span class="label-price">Our price</span>
                        <span class="title-price">$ ${vehicle.salePrice}</span>
                      </div>
                    </div>
                    <div class="vehicle-select">
                      <a href="#" class="btn btn-primary" data-id="vehicle-${
												vehicle.id
											}">Select</a>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="vehicle-detail">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae mollis velit. Nullam interdum auctor turpis, at eleifend tortor pellentesque nec. Fusce pretium nulla ut risus egestas, quis dapibus eros condimentum. Integer volutpat posuere quam vitae mollis. Proin diam sapien, luctus ut faucibus sit amet, dignissim a arcu.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>`;
				$(".vehicle-list-group").append(markup);
			}
			$(".vehicle-select a").on("click", function(e) {
				e.preventDefault();
				setNull(PRODUCT_OBJ);
				var selected_vehicle = productOptions[$(this).data("id")];
				var extras = selected_vehicle.productOptionAdditionalServicesList;
				EXTRA_OBJ.length = 0;
				FINAL_PRICE = selected_vehicle.salePrice;

				//Update the price holder
				PRICE_HOLDER.update(FINAL_PRICE);
				PRODUCT_OBJ["product_id"] = product.id;
				PRODUCT_OBJ["product_option_id"] = selected_vehicle.id;
				if (extras.length) {
					extras.map(option => EXTRA_OBJ.push(option));
					fillExtrasList();
				} else productFormNextStep(".vehicle-list-group", ".final-form-group"); // Extras sayfasına geçiş
			});
			$(".vehicle-more-info a").on("click", function(e) {
				e.preventDefault();
				$(this).toggleClass("active");
				$(this)
					.parents(".vehicle-content")
					.find(".vehicle-detail")
					.slideToggle();
			});
		})();

		//Init price holder
		(function initPrice(element) {
			var options = {
				useEasing: true,
				useGrouping: true,
				separator: ",",
				decimal: ".",
				prefix: "$ "
			};
			PRICE_HOLDER = new CountUp(element, 0, 0, 1, 2.5, options);

			if (!PRICE_HOLDER.error) {
				PRICE_HOLDER.start();
			} else {
				console.error(PRICE_HOLDER.error);
			}
			$("#final-price-container").animate(
				{
					top: "0%",
					opacity: 1
				},
				{
					step: function(now, mx) {
						scale = 1 - (1 - now) * 0.2;
						var top = -(1 - now) * 40 + "%";
						//3. increase opacity of next_fs to 1 as it moves in
						opacity = 1 - now;
						$("#final-price-container").css({ top: top, opacity: opacity });
					},
					duration: 1500,
					complete: function() {
						animating = false;
					},
					//this comes from the custom easing plugin
					easing: "easeInOutBack"
				}
			);
		})("countUp");
		//Render productOptions and trigger next step function
		function fillExtrasList(oneWay = product.oneWay) {
			$(".extras-element-group").empty();

			EXTRA_OBJ.map(extra => {
				const markup = `<div class="animate-box extras-element list-group-item col-sm-12">
          <div class="row">
            <div class="col-sm-9">
              <div class="extra-title">
                <h5>${extra.additionalServiceName}</h5>
              </div>
              <div class="extra-definition">
                <span>${extra.definition ||
									"Suitable for toddlers weighing 9-18 kg (approx 1 to 6 years)."}</span>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="row">
                <div class="form-group col-xs-6">
                  <label for="select1">Outward</label>
                  <select class="form-control" id="select1" data-cost=${
										extra.cost
									} data-id=${extra.id} data-price=0>
                    <option>0</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                ${(oneWay => {
									console.log(oneWay, "onur");
									if (!oneWay)
										return `<div class="form-group col-xs-6"><label for="select2">Return</label> <select class="form-control" id="select2" data-cost=${
											extra.cost
										} data-id=${
											extra.id
										} data-price=0> <option>0</option> <option>1</option> <option>2</option> <option>3</option> <option>4</option> <option>5</option> </select> </div>`;
									else return ``;
								})(oneWay, extra)}
              </div>
            </div>
          </div>
        </div>`;
				$(".extras-element-group").append(markup);
			});

			productFormNextStep(".vehicle-list-group"); // Extras sayfasına geçiş

			$(".extras-element select").on("change", function(e) {
				e.preventDefault();
				onChangeExtras(this);
			});

			function onChangeExtras(obj) {
				var totalSum = FINAL_PRICE;
				var prevPrice = parseFloat($(obj).data("price"));
				var cost = parseFloat($(obj).data("cost"));
				var amount = parseFloat($(obj).val());
				var newPrice = cost * amount;

				console.log(totalSum, "prevSum");
				console.log(prevPrice, "prevPrice");
				console.log(cost, "cost");
				console.log(amount, "amount");
				console.log(newPrice, "newPrice");

				FINAL_PRICE = totalSum + (newPrice - prevPrice);
				//Update the price holder
				PRICE_HOLDER.update(FINAL_PRICE);
				$(obj).data("price", newPrice);
			}
		}
		//Before changing screen to Final form, fills the location info
		function showTransferDetailForm() {
			$("#formArrivalTerminal").text(product.terminalValue);
			$("#formArrivalDestination").text(product.destinationValue);

			if (!product.oneWay) {
				$(".return-trip-form")
					.show()
					.find("input.required")
					.prop("required", true);
				$("#formReturnTerminal").text(product.terminalValue);
				$("#formReturnDestination").text(product.destinationValue);
			} else
				$(".return-trip-form")
					.hide()
					.find("input.required")
					.prop("required", false);

			productFormNextStep(".extras-list-group"); // Transfer detail form sayfasına geçiş
		}
		function fillBookingObject() {
			let tripDetails = {};
			setNull(BOOKING_OBJ);

			const customerName = $("#customer-name").val();
			const dropOffAddress = $("#outward-dropoff").val();
			const customerEmail = $("#customer-email").val();
			const customerPhone = $("#customer-phone").val();
			const customerContactChannel = $("#select-pref").val();

			BOOKING_OBJ["id"] = 0;
			BOOKING_OBJ["groupSize"] = product.groupSize;
			BOOKING_OBJ["customerName"] = customerName;
			BOOKING_OBJ["hotelName"] = $("#outward-dropoff-point").val();
			BOOKING_OBJ["hotelAddress"] = $("#outward-dropoff-addr").val();
			BOOKING_OBJ["serviceDate"] = new Date().toISOString();
			BOOKING_OBJ["bookingDate"] = product.groupSize;
			BOOKING_OBJ["departurePickupDate"] = product.groupSize;
			BOOKING_OBJ["flightNumber"] = $("#outward-number").val();
			BOOKING_OBJ["salePrice"] = FINAL_PRICE;
			BOOKING_OBJ["reservationStatusId"] = 1;
			BOOKING_OBJ["agentId"] = agent ? agent.id : 3;
			BOOKING_OBJ["customerContactPhone"] = customerPhone;
			BOOKING_OBJ["customerContactChannelId"] = customerContactChannel;
			BOOKING_OBJ["productId"] = PRODUCT_OBJ.product_id;
			BOOKING_OBJ["productOptionId"] = PRODUCT_OBJ.product_option_id;
			BOOKING_OBJ["paymentDueDate"] = "";
			BOOKING_OBJ["selectedAdditionalServices"] = 0;
			BOOKING_OBJ["isArrival"] = true;
			BOOKING_OBJ["currencyId"] = 6;

			if (!product.oneWay) {
				tripDetails.bookingGroupId = guidCreator();
				BOOKING_OBJ["bookingGroupId"] = tripDetails.bookingGroupId;
				tripDetails.return = {
					flightNumber: $("#return-number").val(),
					hotelName: $("#return-pickup-point").val(),
					hotelAddress: $("#return-dropoff-addr").val(),
					selectedAdditionalServices: 0
				};
			}

			return tripDetails;
		}
		function guidCreator() {
			return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(
				c
			) {
				var r = (Math.random() * 16) | 0,
					v = c == "x" ? r : (r & 0x3) | 0x8;
				return v.toString(16);
			});
		}
		function submitBookingOrder(bookingGroupObject) {
			var isMyObjectEmpty = !Object.keys(bookingGroupObject).length;
			var data = JSON.stringify(BOOKING_OBJ);
			console.log(data);
			fetch(URL + "/api/booking", {
				method: "POST",
				headers: {
					accept: "application/json",
					"content-type": "application/json"
				},
				body: data
			})
				.then(r => {
					console.log(r.body);
					return r.json();
				})
				.then(r => console.log(r))
				.catch(err => console.log(err));
		}
		function productFormNextStep(current_fs, nextStep = "") {
			if (animating) return false;
			animating = true;
			current_fs = $(current_fs);
			next_fs = nextStep ? $(nextStep) : current_fs.next();

			current_fs.toggleClass("active");
			next_fs.toggleClass("active");

			//activate next step on progressbar using the index of next_fs
			// $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

			//show the next fieldset
			next_fs.show();
			//hide the current fieldset with style
			current_fs.animate(
				{
					opacity: 0
				},
				{
					step: function(now, mx) {
						//as the opacity of current_fs reduces to 0 - stored in "now"
						//1. scale current_fs down to 80%
						scale = 1 - (1 - now) * 0.2;
						//2. bring next_fs from the right(50%)
						left = now * 50 + "%";
						//3. increase opacity of next_fs to 1 as it moves in
						opacity = 1 - now;
						current_fs.css({
							transform: "scale(" + scale + ")",
							position: "absolute"
						});
						next_fs.css({ left: left, opacity: opacity });
					},
					duration: 800,
					complete: function() {
						current_fs.hide();
						animating = false;
						contentWayPoint();
					},
					//this comes from the custom easing plugin
					easing: "easeInOutBack"
				}
			);
		}
		function productFormGoBack(current_fs, prevStep = "") {
			console.log("meraba");
			if (animating) return false;
			animating = true;
			current_fs = $(current_fs);
			prev_fs = prevStep ? $(prevStep) : current_fs.prev();

			current_fs.toggleClass("active");
			prev_fs.toggleClass("active");

			//de-activate current step on progressbar
			// $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

			//show the previous fieldset
			prev_fs.show();
			//hide the current fieldset with style
			if (prev_fs[0] != current_fs[0]) {
				$("#search-results .list-group").css({
					position: "initial",
					transform: "none"
				});
				current_fs.animate(
					{
						opacity: 0
					},
					{
						step: function(now, mx) {
							//as the opacity of current_fs reduces to 0 - stored in "now"
							//1. scale prev_fs from 80% to 100%
							scale = 0.8 + (1 - now) * 0.2;
							//2. take current_fs to the right(50%) - from 0%
							left = (1 - now) * 50 + "%";
							//3. increase opacity of prev_fs to 1 as it moves in
							opacity = 1 - now;
							current_fs.css({ left: left });
							prev_fs.css({
								transform: "scale(" + scale + ")",
								opacity: opacity
							});
						},
						duration: 800,
						complete: function() {
							current_fs.hide();
							animating = false;
							contentWayPoint();
						},
						//this comes from the custom easing plugin
						easing: "easeInOutBack"
					}
				);
			} else {
				current_fs.fadeTo(400, 0, function() {
					prev_fs.fadeTo(400, 1, contentWayPoint());
				});
			}
		}
	};

	$(function() {
		var path = window.location.pathname;
		datePicker();
		timePicker();
		sliderInit();
		searchTransfer();
		typeSuggestion(
			URL + "/api/destination",
			"#destination",
			"destinationValue",
			["id"]
		);
		typeSuggestion(URL + "/api/terminal", "#airport", "terminalValue", [
			"id",
			"cityId"
		]);
	});
})();
